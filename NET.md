# Configuration réseau

Historiquement : la configuration réseau était stocké dans
des fichiers plat (texte) :

- RHEL : `/etc/sysconfig/network-scripts/ifcg-<interface>`
- Debian : `/etc/network/interfaces` et `/etc/network/interfaces.d/`

Transition en cours vers Network Manager :

- Il prend en compte les fichiers plats
- Il a sa propre base de données, gérée avec `nmtui` et `nmcli`

Pour expérimenter : ajoutons à nos VMs une interface réseau qui
les relie entre eux.

Virtual Box : hélas il faut éteindre les VMs

Commencez par supprimer toutes les interfaces avec `nmtui`.

On va toutes les reconfigurer avec `nmcli` (remplacer 10.6 par 10.1, 10.2, etc.)

~~~~Bash
$ sudo nmcli device status
$ sudo nmcli connection add ifname enp0s3 type ethernet ipv4.method auto 
$ sudo nmcli connection up ethernet-enp0s3 
$ sudo nmcli connection add ifname enp0s8 type ethernet ipv4.method manual ipv4.addresses 10.6.0.1/24
$ sudo nmcli device status
DEVICE  TYPE      STATE                  CONNECTION      
enp0s3  ethernet  connecté               ethernet-enp0s3 
enp0s8  ethernet  connecté               ethernet-enp0s8 
lo      loopback  connecté (en externe)  lo
$ ip a
$ ping 10.6.0.x # où x est l'autre
~~~~

Même chose sur l'autre VM : avec 10.6.0.2 (remplace 10.6 par 10.1, 10.2, etc.)

TP : Configurer NGINX en proxy inverse sur la Debian 12 pour qu'il serve les pages
fournis par Apache2 sur la Debian 11 : s'inspirer de :

cf. https://framagit.org/jpython/formation-elk

À partir de : "A more secure way to connect to Kibana from the outside"

(adapter la section realserver et servername)

Vous devriez pouvoir voir votre site à partir de Windows à l'url :
`http://votre_ip_du_réseau_m2i` :

- NGINX va rediriger en https
- Et montrer la page par défaut d'Apache sous Debian

## À tester : faire proxy vers un Apache en https avec ses propres certificats

Pour la partie Apache, on peut se référer à :

- https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-20-04-fr


