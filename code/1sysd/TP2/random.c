#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
	int n;

	n = rand();
	printf("%d\n", n);
        n = n % 10 + 1;
	/* chez moi c'est tout le temps ... 4 */
	printf("%d\n", n);
	n = rand() % 10 + 1;
	/* chez moi c'est tout le temps ... 7 */
	printf("%d\n", n);
	n = rand() % 10 + 1;
	/* chez moi c'est tout le temps ... 8 */
	printf("%d\n", n);

	/* Il faut initialiser le générateur pseudo-alétoire 
	 * avec le nb de seconde depuis 01/01/1970 à 00h00mn
	 * par exemple */
	printf("Après avoir pris une graine qui est l'epoch\n");
	srand(time(NULL)); // man srand et man time

	n = rand() % 10 + 1;
	printf("%d\n", n);
	n = rand() % 10 + 1;
	printf("%d\n", n);
	n = rand() % 10 + 1;
	printf("%d\n", n);
	n = rand() % 10 + 1;
	printf("%d\n", n);
	n = rand() % 10 + 1;
	printf("%d\n", n);

	return 0;
}
