#!/usr/bin/env python3

import sys

val1, val2, op = sys.argv[1::]

# idem que : val1 = sys.argv[1] 
#            val2 = sys.argv[2]
#              op = sys.argv[3] 

val1 = float(val1)
val2 = float(val2)

# switch/case existe en Python 3 depuis
# très peu de temps on utilise if/elif/else

if op == "+":
    print(val1 + val2)
elif op == '-':
    print(val1 - val2)
elif op == '*':
    print(val1 * val2)
elif op == '/':
    print(val1 / val2)
else:
    print("Opération inconnue.")
