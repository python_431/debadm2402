#include<stdlib.h>

extern int slen(char *s);

char *scopy(char *s) {
    char *dest, *p;
    dest = malloc(sizeof(char) * (slen(s) + 1));
    if (dest == NULL) {
        // pb d'allocation mémoire
        return NULL;
    }
    p = dest;
    // Deux façons équivalentes :
    //while ( ( *p++ = *s++ ) ) ; // double () pour éviter warning
    while (*s) {
        *p++ = *s++;
    }
    *p = 0; // zéro terminal à ajouter !
    return dest;
}

int sequal(char *s1, char *s2) {
    // Tant que : s1 pas fini ET s2 pas fini 
    // ET *s1 == *s2 (caract. courant identique)
    while ( *s1 && *s2 && *s1 == *s2 ) {
        s1++;
        s2++;
    }
    // On peut être sorti pour plusieurs raisons :
    // fin de s1 et pas de s2 : *s1 est 0 et *s2 pas 0
    // fin de s2 et pas de s1 : *s1 pas 0 et *s2 est 0
    // fin d'aucun des deux, on a trouvé un caractère
    // différent : *s1 est différent de *s2
    //     dans tous les cas : *s1 est différent de *s2
    // autre seul cas : fin de s1 et fin de s2, donc 
    //     ils sont identiques (et 0). 
    // pour le return au choix : (même résultat)
    // return *s1 == 0 && *s2 == 0; // ou :
    return *s1 == *s2;
}

