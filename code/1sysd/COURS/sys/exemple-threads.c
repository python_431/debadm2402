// ------------------------------------------------------------------
// exemple-threads.c
// Fichier d'exemple du livre "Solutions Temps-Reel sous Linux"
// (C) 2012 - Christophe BLAESS
// http://christophe.blaess.fr
// ---------------------------------------------------------------------

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void * factorielle(void * arg)
{
	int i;
	int n = (int) arg;
	int resultat = 1;
	for (i = 2; i <= n; i ++) {
		resultat = resultat * i;
		fprintf(stderr, "%d! : en calcul...\n", n);
		sleep(1);
	}
	return (void *) resultat;
}


int main(int argc, char * argv[])
{
	pthread_t * threads = NULL;
	void * retour;
	int i;
	int n;
	// Verification des arguments
	if (argc < 2) {
		fprintf(stderr, "usage: %s valeurs...\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	// Allocation d'un tableau d'identifiants 
	threads = calloc(argc-1, sizeof(pthread_t));
	if (threads == NULL) {
		perror("calloc");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr, "main(): lancement des threads\n");
	for (i = 1; i < argc; i ++) {
		// Recuperation de l'argument numerique
		if (sscanf(argv[i], "%d", & n) != 1) {
			fprintf(stderr, "%s: invalide\n", argv[i]);
			exit(EXIT_FAILURE);
		}
		// Lancement du thread
		if (pthread_create(& (threads[i-1]), NULL, factorielle, (void *) n) != 0) {
			fprintf(stderr, "Impossible de demarrer le thread %d\n", i);
			exit(EXIT_FAILURE);
		}
	}
	fprintf(stderr, "main(): tous threads lances\n");
	for (i = 1; i < argc; i ++) {
		// Attente du thread
		pthread_join (threads[i-1], & retour);
		fprintf (stderr, "main(): %s! = %d\n", argv[i], (int) retour);
	}
	fprintf(stderr, "main(): tous threads termines\n");
	free(threads);
	return EXIT_SUCCESS;
}

