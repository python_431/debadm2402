// La façon de créer un nouveau processus 
// sous UNIX :
// une fonction : fork()
// duplique le processus courant
// if (pid = fork()) {
//     // pid est vrai, non zéro, on est le processus parent
//     printf("ici le process parent");
//     // le parent peut faire un wait sur le pid du fils
//     
//  } else {
//     // pid est faux, zéro, on est le processus fils
//     // on peut utiliser getpid qui renvoie son propre id
//  }

// écrire un programme qui se duplique et affiche s'il est
// le père ou le fils
// généraliser à un processus qui en lance 5 autres
// utiliser la fonction sleep() pour qu'il dorment entre
// un message de lancement et un message d'arrêt
//
//
//$ ./process
//Ici le parent
//Ici le fils
//$ ./process
//ici le parent
//ici un fils pid 1123
//...
//un fils se termine ...

#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>

int main() {
    int pid, me;
    if ((pid = fork())) {
        // processus parent
        printf("Ici le processus parent\n");
        printf("Processus fils lancé : %d\n", pid);
        printf("Parent dort 5s\n");
        sleep(5);
        printf("Parent se termine...\n");
    } else {
        // processus descendant
        me = getpid();
        printf("Ici le processus fils : mon PID est %d\n", me);
        printf("Fils dort 2s\n");
        sleep(2);
        printf("File se termine...\n");
    }
}
