#include<stdio.h>

int main() {
    int n, i, s;
    int numbers[5];

    printf("Merci de saisir 5 nombres entiers\n");
    s = 0;
    for (i = 0; i < 5; i++) {
        printf("nombre n°%d : ", i + 1);
        scanf("%d", &n);
        numbers[i] = n;
        s += n;
    }
   
    printf("Somme : %d\n", s);
    
    printf("Vous avez saisi :");
    for (i = 0; i < 5; i++) {
        printf(" %d", numbers[i]);
    }
    printf("\n");
}
