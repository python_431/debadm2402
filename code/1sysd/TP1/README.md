# TP1 (suite) :

## Créer un nouveau fichier source : bonjour.c

Qui contient du code pour : 

1. Afficher "Bonjour tout le monde !"
2. En passant à la ligne : (il suffit d'ajouter \n à
   la fin de la chaîne (avant le " fermant)
3. Ajouter dans le Makefile un nouveau bloc (insérez
   d'abord une ligne vide à la fin) qui indique
   que le fichier bonjour dépends du fichier bonjour.c
   et contient la commande de compilation de bonjour.c
   qui génère le fichier binaire bonjour

Note on peut faire un peu mieux :

~~~~Makefile
.PHONY: all clean

all: bonjour hello

hello: hello.c
	gcc -o hello hello.c

bonjour: bonjour.c
	gcc -o bonjour bonjour.c

clean:
	rm -f hello bonjour
~~~~

Ainsi la comande `make` vise la cible « all » qui visent les
cibles `hello` et `bonjour` définies plus make    

Et en plus `make clean` efface tous les binaire produits.

4. ajouter à la fin de la fonction main (avant la
   dernière accolade) ceci : 
     exit(0);
5. Ça déclenche un avertissement à la compilation,
   comment le faire disparaître ?

comme indiqué : ajouter `#include <stdlib.h>`

6. Comment faire pour que le fichier bonjour.c soit
   dans le dépot git et visible par tous ? 

~~~~Bash
$ git add bonjour.c
$ git commit -a -m "ajout de bonjour.c"
$ git push
~~~~
