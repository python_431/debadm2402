# Cache DNS et resolv.conf

https://www.malekal.com/configurer-cache-dns-linux-systemd-resolved-dnsmasq-bind/

Comment avoir un cache dns local : systemd-resolved ou dnsmasq.

## Avec systemd-resolved

~~~~Bash
$ sudo apt install resolvconf
$ sudo apt install systemd-resolved
$ sudo systemctl start systemd-resolved
$ sudo systemctl enable systemd-resolved
$ resolvectl status
~~~~

Hypothèse : ajouter `nameserver 127.0.0.53` dans `head` fait en
sorte que le cache de systemd-resolved est utilisé en pratique.

En configuration sans dhcp : ajouter vos DNS statique dans 
`/etc/systemd/resolved.conf` ; ça devrait marcher

Avec `dnsmasq` (sans systemd-resolved) le même principe devrait
marcher.

- resolvconf est-il vraiment pertinent ?

(nécessaire ici pour tester dans la salle de cours)
On peut ignorer le dns renvoyé par le dhcp :

~~~~Bash
$ sudo nmcli connection modify ethernet-enp0s3 ipv4.ignore-auto-dns yes
~~~~

